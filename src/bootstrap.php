<?php

require 'config.php';

spl_autoload_register(
  function ($name) {
    $filePath = str_replace('\\',DIRECTORY_SEPARATOR, APPLICATION_PATH.DS.$name.'.php');
  }
);

$uri = $_SERVER['REQUEST_URI'];

try {
  require 'routes.php';
} catch (Exception $exception) {
    echo "Konstatēta kļūda datu ievadē: ",$exception->getMessage(), "\n";
}
