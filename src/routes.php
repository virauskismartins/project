<?php

require '/var/www/html/src/Model/Domain/Service/calendar.php';

$route = $_SERVER['REQUEST_URI'];

switch($route){
  case '/':
      require '/var/www/html/src/Application/index.php';
  break;

  case '/galerija':
      require '/var/www/html/src/Application/galerija.php';
  break;

  case "/$curr":
      require '/var/www/html/src/Application/rezervacija.php';
  break;

  case "/$next":
      require '/var/www/html/src/Application/rezervacija.php';
  break;

  case "/$prev":
      require '/var/www/html/src/Application/rezervacija.php';
  break;

  case '/save':
      require '/var/www/html/src/Model/Domain/Service/reservationSender.php';
      header("Location: "." /".date('Y-m'));
  break;

  case '/delete':
    session_start();
    if (!empty($_SESSION['loggedin'])) {
      require '/var/www/html/src/Model/Domain/Service/delete.php';
    }
    else {
      header('Location: /default');
    }
  break;

  case '/deletegallery':
    session_start();
    if (!empty($_SESSION['loggedin'])) {
      require '/var/www/html/src/Model/Domain/Service/deletegallery.php';
    }
    else {
      header('Location: /default');
    }
  break;

  case '/home';
    require 'Application/home.html';
  break;

  case '/jaunumi':
    require 'Application/jaunumi.php';
  break;

  case '/login':
    require 'Application/login.php';
  break;

  case '/authenticate':
    require '/var/www/html/src/Model/Domain/Service/authenticate.php';
  break;

  case '/ImageInDatabse':
    session_start();
    if (!empty($_SESSION['loggedin'])) {
      require '/var/www/html/src/Model/Domain/Service/ImageInDatabase.php';
    }
    else {
      header('Location: /default');
    }
  break;

  case '/error':
    require '/var/www/html/src/Model/Domain/Service/error.php';
  break;

 case '/cms':
  session_start();
  if (!empty($_SESSION['loggedin'])) {
 	  require '/var/www/html/src/Application/cms.php';
 	}
 	else {
    header('Location: /default');
  }
  break;
  die;

  case '/reservationcms':
   session_start();
   if (!empty($_SESSION['loggedin'])) {
     require 'Application/reservationcms.php';
   }
   else {
     header('Location: /default');
   }
   break;

   case '/deletereservation':
    session_start();
    if (!empty($_SESSION['loggedin'])) {
      require 'Application/deletereservation.php';
    }
    else {
      header('Location: /default');
    }
    break;

  default:
    echo "<h1>404 Page not found<h1>";
  }
