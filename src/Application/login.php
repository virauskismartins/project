<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
          <meta name="viewport" content="width-device-width, initial-scale=1.0">
        <title>Pieslēgšanās CMS</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css%22%3E">
        <link rel="stylesheet" type="text/css" href="../../public/assets/css/layout.css">
    </head>
    <body>
        <div class="login">
            <div id="title-login"><h1>Pieslēgties</h1></div>
            <form action="/authenticate" method="post">
                <label for="username">
                    <i class="fas fa-user"></i>
                </label>
                <input type="text" name="username" placeholder="Lietotājvārds" id="username" required>
                <label for="password">
                    <i class="fas fa-lock"></i>
                </label>
                <input type="password" name="password" placeholder="Parole" id="password" required>
                <input id="login-btn" type="submit" value="Pieslēgties">
            </form>
        </div>
    </body>
</html>
