<!doctype html>
<html>
    <head>
        <title>Galerija</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width-device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../../public/assets/css/layout.css">
        <link rel="stylesheet" type="text/css" href="../../public/assets/css/lightbox.min.css">
        <script src="../../public/assets/js/lightbox-plus-jquery.min.js"></script>
    </head>
    <body>
      <form action="action.php" method="post">
        <div class="container">
          <div class="row">
             <div class="col-12"><h1 class="title">Vizbulītes</h1></div>
             <div class="col-12">
               <div class="topnav">
                 <a href="/">Sākums</a>
                   <a href="jaunumi">Jaunumi</a>
                 <a class="active" href="galerija">Galerija</a>
                 <a href="<?php echo "/".date('Y-m');?>">Rezervācija</a>
               </div>
             </div>
             <div class="gallery">
            <?php
            $db = new mysqli(PDO_HOST, PDO_USERNAME, PDO_PASSWORD, PDO_DB);

            $sql = "SELECT image FROM images";
            if($result = mysqli_query($db, $sql)){
              if(mysqli_num_rows($result) > 0){
                while($row = mysqli_fetch_array($result)){
                  ?><a href="<?php echo  $row['image'] ;?> "
                   data-lightbox="mygallery" style="text-decoration: none"/>
                 <img src="<?php echo  $row['image'] ;?>"/><?php
                }
              }
            }
            ?>
            </div>
          </div>
        </div>
      </form>
    </body>
</html>
