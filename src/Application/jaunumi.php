<!doctype html>
<html>
  <head>
    <title>Jaunumi</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../public/assets/css/layout.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <form action="action.php" method="post">
    <div class="container">
      <div class="row">
        <div class="col-12"><h1 class="title">Vizbulītes</h1></div>
        <div class="col-12">
          <div class="topnav">
          <a href="/">Sākums</a>
          <a class="active"  href="jaunumi">Jaunumi</a>
          <a href="galerija">Galerija</a>
          <a href="<?php echo "/".date('Y-m');?>">Rezervācija</a>
          </div>
        </div>
      </form>
<?php
  $con=mysqli_connect(PDO_HOST,PDO_USERNAME, PDO_PASSWORD,PDO_DB);
  $sql = "SELECT * FROM news_images ORDER BY id_new_image DESC";
  $result = $con->query($sql);

if ($result->num_rows > 0){
  while($row = $result->fetch_assoc())
  {?>
  <div class="news-content">
  <div class="title-news"><h3><?php echo $row["title"];?></h3></div>
  <div class="image-news"><img class="image"
  style="text-align: center;" src="<?php echo  $row['image'] ;?>"/></div>
  <div class="text-news"><p><?php echo $row["new"];?></p></div>
  </div><?php
  }
  $con->close();
}
?>
  </body>
</html>
