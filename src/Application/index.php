<!doctype html>
<html>
  <head>
    <title>Vizbulītes</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../public/assets/css/layout.css">
    <style>
    .fixed-bg {
    background-image: url("../../public/assets/img/fons.jpg");
    width: 100%;
    min-height: 100px;
    background-attachment: fixed;
    background-position: center;
    background-size: cover;
    }
    </style>
</head>
    <body>
        <form method="post">
          <div class="dala">
          <div class="topnav">
              <a class="active" href="/">Sākums</a>
              <a href="jaunumi">Jaunumi</a>
              <a href="galerija">Galerija</a>
              <a href="<?php echo "/".date('Y-m');?>">Rezervācija</a>
          </div></div>
            <div class="container">
              <div class="row">
                <div class="fixed-bg">
                  <div class="img4">
                    <img src="http://localhost/public/assets/img/lv.png">
                    <a href="home"><img src="http://localhost/public/assets/img/uk.png"> </a>
                  </div>
                  <div class="daeh">
                    <daeh>Viesu mājas "Vizbulītes"</daeh>
                  </div>
                  <div class="row">
                  <div class="dala">
                    <div class="head2">
                    <head2>Mūsu Piedāvajumi</head2>
                    </div>
                  </div>
                  <div class="dala3">
                    <div class="transbox">
                    <div class="img2">
                    <img src="http://localhost/public/assets/img/bed.png">
                    </div>
                    <div class="head">
                    <head>Guļvieta</head>
                    </div>
                    <p class="pp">Ir piejamas 3 istabas ar divām gultām,
                    3 istabas ar vienvietīgajām gultām un viena istaba
                    ar divvietīgu gultu.</p>
                    </div>
                  </div>
                  <div class="dala3">
                    <div class="transbox">
                    <div class="img2">
                    <img src="http://localhost/public/assets/img/sauna.png">
                    </div>
                    <div class="head">
                    <head>Sauna</head>
                    </div>
                    <p class="pp">Sauna ir piejama 8 cilvēkiem,
                    blakus ir arī aukstais baseins un duša.</p>
                    </div>
                  </div>
                  <div class="dala3">
                    <div class="transbox">
                    <div class="img2">
                    <img src="http://localhost/public/assets/img/bed.png">
                    </div>
                    <div class="head">
                    <head>Banketa Zāle</head>
                    </div>
                    <p class="pp">Banketa zāle 60 cilvēkiem</p>
                    </div>
                  </div>
                  <div class="dala2">
                    <div class="transbox">
                    <div class="img3">
                    <img src="http://localhost/public/assets/img/wifi.png">
                    </div>
                    <div class="head">
                    <head>internets</head>
                    </div>
                    <p class="pp">Piejams bezmaksas Wi-Fi internets</p>
                    </div>
                  </div>
                  <div class="dala2">
                    <div class="transbox">
                    <div class="img3">
                    <img src="http://localhost/public/assets/img/pond.png">
                    </div>
                    <div class="head">
                    <head>Dīķis</head>
                    </div>
                    <p class="pp">Pie viesu nama blakus ir dīķis,
                    kur ir arī atļauts makšķerēt (ja nav makšķeres,
                    ir pieejamas arī tās)</p>
                    </div>
                  </div></div>
                </div>
              </div>
            </div>
        </form>
        <footer class="feet">
          <div class="map">
          <iframe class="map2"
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2185.311353780466!2d24.289459315942135!3d56.78912598084318!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e929d000000001%3A0x7895e2b25e2b8946!2sVizbul%C4%ABtes!5e0!3m2!1slv!2slv!4v1582529595990!5m2!1slv!2slv"></iframe>
          </div>
          <div class="dala2">
          <p>Telefona numurs: 20202020</p><br>
          <p>E-pasts: example@example.lv</p>
          </div>
        </footer>
    </body>
</html>
