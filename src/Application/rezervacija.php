<?php
require '/var/www/html/src/Model/Domain/Service/calendar.php';
?>
<!doctype html>
<html>
    <head>
        <title>Rezervācija</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../../public/assets/css/layout.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">

              <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    </head>
    <body>
        <div class="col-12"><h1 class="title">Vizbulītes</h1></div>
        <div class="topnav">
          <a href="/">Sākums</a>
          <a href="jaunumi">Jaunumi</a>
          <a href="galerija">Galerija</a>
          <a class="active" href="<?php echo "/".date('Y-m');?>">Rezervācija</a>
        </div>
        <div class="container-res">
              <style>
                  .container {
                      font-family: 'Noto Sans', sans-serif;
                      margin-top: 80px;
                  }
                  h3 {
                      margin-bottom: 30px;
                  }
                  /*
                  .today {
                      background: orange;
                  }
                  th:nth-of-type(1), td:nth-of-type(1) {
                      color: red;
                  }
                  th:nth-of-type(7), td:nth-of-type(7) {
                      color: blue;
                  }
                  */
              </style>
              <div class="container-cal">
                  <h3><a href="/<?php echo $prev; ?>">&lt;
                  </a> <?php echo $html_title; ?> <a href="/<?php echo $next; ?>">&gt;</a></h3>
                  <table class="table table-bordered">
                      <tr>
                          <th>P</th>
                          <th>O</th>
                          <th>T</th>
                          <th>C</th>
                          <th>P</th>
                          <th>S</th>
                          <th>S</th>
                      </tr>

                      <?php
                          foreach ($weeks as $week) {
                              echo $week;
                          }
                      ?>
                  </table>
              </div>
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <div class="res-info">
      <h3>Viesu nama rezervācija</h3>
    </div>
    <form action="/save" method="post">
    <div class="col-12">
      <div class="reservation">
      <input class="first-name-text" type="text" placeholder="Vārds" name="first-name-text">
      <input class="last-name-text" type="text" placeholder="Uzvārds" name="last-name-text">
      <input class="e-mail-text" type="text" placeholder="E-pasts" name="e-mail-text">
      <input class="phone-text" type="text" placeholder="Telefona numurs" name="phone-text">
      <input style="display:none;" class="res-date-text" type="date" placeholder="Rezervācijas datums" name="res-date-text">
      <input class="guest-count-text" type="number" placeholder="Viesu skaits" name="guest-count-text">
    </div>
    <input type="submit" name="submit" value="Nosūtīt reģistrāciju" id="send-res">
  </div>
</form>
  </div>

</div>
          </div>
        </div>

      <script>
  // Get the modal
  var modal = document.getElementById("myModal");

  // Get the button that opens the modal
  var btn = document.getElementById("myBtn");
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  var calendarDays = document.getElementsByClassName("calendar-day");

$(function() {

  $('.calendar-day').click( function() {
      //console.log($(this).attr('data-date'));

      let calendarDate = $(this).attr('data-date');//lets caledar class get clicked day date

      $('#myModal').fadeIn(500);//animation for modal form
      $('#myModal .res-date-text').val(calendarDate);//opens modal form and inputs date in it
  });
})

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
  </script>
    </body>
</html>
