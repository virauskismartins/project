<!doctype html>
  <html>
    <head>
        <meta name="viewport" content="width-device-width, initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="../../public/assets/css/layout.css">
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
       <script src="../../public/assets/js/lightbox-plus-jquery.min.js"></script>
       <title>Content Managment System</title>
    </head>
    <body>
      <div class="container-cms">
      <form action="/ImageInDatabse" method="post" enctype="multipart/form-data">
        <div class="top-row">
          <label for="type-switch">Datu ievade</label>
            <select id="type-switch" name="type">
                <option value="" selected disabled hidden>Datu tips</option>
                <option value="1">Galerija</option>
                <option value="2">Jaunumi</option>
            </select>
        </div>



        <div id="image-container" class="switch-container">
          <div class="gal">
            <label class="image-label" for="image1-selection">Attēls: </label>
            <button class="img-button" for="file" onclick="document.getElementById('file').click(); return false;">Izvēlēties attēlu</button>
            <input id="file" style="visibility:hidden;" type="file" name="image1-field" id="image1-selection"/>
            <div class="suggest"><a><br>Lai galerija labāk izskatītos izmantot vienāda lieluma attēlus</a></div>
          </div>
        </div>

        <div id="news2-container" class="switch-container">
          <div class="newz">
            <label class="title-label" for="title1-selection">Virsraksts: </label>
            <input type="text" name="title1-field" id="title1-selection"/><br>
            <label id="news-label" for="text3-selection"><Br>Jaunumi: </label>
            <textarea rows="1" cols="50" wrap="physical" class="cms-input" name="text3-field" id="text3-selection"></textarea><br>
            <label class="img-button-label" for="image2-selection">Attēls :</label>
            <button class="img-button" for="file1" onclick="document.getElementById('file1').click(); return false;">Izvēlēties attēlu</button>
            <input id="file1" type="file" style="visibility:hidden;" name="image2-field" id="image2-selection"/>
          </div>
        </div>
          <input class="save" type="submit" name="submit" value="Saglabāt"/>
  </form>

  <form action="/reservationcms" method="post" enctype="multipart/form-data">
    <div class="top-row">

        <input type="submit" name="submitrezervation" value="Rezervācījas"/>

    </div>
  </form>
  <script>
    const newsField = $("#news-container");
    const imageField = $("#image-container");
    const news2Field = $("#news2-container");
    const title1Field = $("#title1-selection");
    const image1Field = $("#image1-selection");
    const image2Field = $("#image2-selection");
    const text3Field = $("#text3-selection");

    $("#news2-container").hide();
    $("#image-container").hide();

    function hideSwitchElements() {
      $("#news2-container").hide();
      $("#image-container").hide();
    }

function selectionResolver(selection) {
  switch (selection) {
    case "1":
      hideSwitchElements();
      $("#image-container").show();
      $("#news2-container").find('input').val('');
      break;
    case "2":
      hideSwitchElements();
      $("#news2-container").show();
      $("#news-container").find('input').val('');
      break;
  }
}

$('#type-switch').on('change', function () {
  selectionResolver($(this).val());
});
</script>
</form>

<form  name="frmpost" method="post" action="">
  <div class="cms-container">
    <div class="row">
      <div class="col-12">
        <div class="row">
          <div class="col-10"><h1 class="mass-dlt-title">Jaunumi</h1></div>
          <div class="col-2"><button name="btn_delete" id="btn_delete" class="btn-delete">Dzēst</button></div>
      </div>
      </div>
      </div>
  </div>

  <div class="news-grid">
    <?php
    $db = new mysqli(PDO_HOST, PDO_USERNAME, PDO_PASSWORD, PDO_DB);
    $resulta = mysqli_query($db, "select * from news_images");
    while($row = mysqli_fetch_array($resulta, MYSQLI_ASSOC)):?>
    <div class="news-output">
      <input type="checkbox" name="c1" id = "news" value="<?php echo $row["id_new_image"]; ?>"  />
        <div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="news-title"><b> Virsraksts: </b>
            <?= $row['title'] ?>
            </div>
          </div>
          <div class="col-3"></div>
          <?php if($row["image"] != ""){?>
          <img class="cms-image" src="<?php echo $row['image']; ?>"/>
          <?php } ?>
        </div>
      </div>
      <?php endwhile; ?>
    </div>

<script>
$(document).ready(function(){
$('#btn_delete').click(function(){

var id_new_image = [];

$(':checkbox:checked').each(function(i){
  id_new_image[i] = $(this).val();
});

$.ajax({
  async:false,
  type: 'POST',
  cache:false,
  url:'/delete',

  data:{id_new_image:id_new_image},
 })

.done(function (response) {
  console.log('Ajax delete executed');
});

console.log(id_new_image);

  });
});

</script>

<form name="frmpos" method="post" action="">
  <div class="cms-container">
    <div class="row">
      <div class="col-12">
              <div class="row">
              <div class="col-8"><h1 class="mass-dlt-title">Galerija</h1></div>
              <div class="col-2">
              </div>
              <div class="col-2">
           <button name="btn_deleteb" id="btn_deleteb" class="btn-delete">Dzēst</button>
              </div>
              <div class="line"></div>
              </div>
              </div>
              <div class="col-12">
              </div>
            </div>
          </div>
            <div class="news-grid">
            <?php
            $db = new mysqli(PDO_HOST, PDO_USERNAME, PDO_PASSWORD, PDO_DB);
            $resultb = mysqli_query($db, "select * from images");
            while($rowb = mysqli_fetch_array($resultb, MYSQLI_ASSOC)){
            ?>

            <div class="news-output">
              <input type="checkbox" name="c1" id = "gallery" value="<?php echo $rowb["id_image"]; ?>"  />
              <img class="cms-image" src="<?php echo $rowb['image'] ;?>"/>
            </div>
            <?php } ?>
            </div>
<script>
$(document).ready(function(){
$('#btn_deleteb').click(function(){

var id_image = [];

$(':checkbox:checked').each(function(b){
  id_image[b] = $(this).val();
});

$.ajax({
  async:false,
  type: 'POST',
  cache:false,

  url:'/deletegallery',

  data:{id_image:id_image},
})

.done(function (response) {
  console.log('Ajax delete executed');
});
  console.log(id_image);
  });
});
</script>




    </div>
  </body>
</html>
