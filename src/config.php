<?php
define('DS',DIRECTORY_SEPARATOR);
define('APPLICATION_PATH', dirname($_SERVER['DOCUMENT_ROOT']).DS.'src');
define('TEMPLATE_PATH','SOURCE_PATH'.DS.'Application'.DS.'Template');
define('LOG_PATH',dirname('SOURCE_PATH'.DS.'logs'));

define('PDO_DSN','mysql:host=localhost;charset=utf8;dbname=viesu_nams');
define('PDO_USERNAME','root');
define('PDO_PASSWORD','');
define('PDO_HOST','localhost');
define('PDO_DB','viesu_nams');
