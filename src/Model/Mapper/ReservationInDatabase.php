<?php

namespace Model\Mapper;

use Model\Domain\Primitive\Address;
use Model\Domain\Entity\Reservation;
use Model\Domain\Primitive\Email;
use PDO;

class ReservationInDatabase
{
    protected $database;

    public function __construct()
    {
    	 $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_CASE => PDO::CASE_NATURAL,
        PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING
    ];
      $this->database = new PDO(PDO_DSN,PDO_USERNAME,PDO_PASSWORD,$options);
    }



    public function insert(Reservation $reservation)
    {
      $query = $this->database->prepare("
	      INSERT INTO reservation
      	(first_name,last_name,email,phone,res_date,guest_count)
     		 VALUES
     	(:first_name,:last_name,:email,:phone,:res_date,:guest_count)
      ");
      $query->bindValue(':first_name',$reservation->firstName());
      $query->bindValue(':last_name',$reservation->lastName());
      $query->bindValue(':email',$reservation->email());
      $query->bindValue(':phone',$reservation->phone());
      $query->bindValue(':res_date',$reservation->resDate());
      $query->bindValue(':guest_count',$reservation->guestCount());

      $query->execute();

    }

    public function delete()
    {

    }

    public function fetch(Reservation $reservation){

    }
}
