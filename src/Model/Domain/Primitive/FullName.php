<?php

use Exception;

final class FullName
{
    private $firstName;
    private $lastName;

    public function __construct($firstName,$lastName)
    {
      $this->setFirstname($firstName);
      $this->setLastname($lastName);
    }

    private function setFirstName(string $firstName)
    {
        $this->firstName=$firstName;
    }

    public function firstName() : string
    {
      return $this->firstName;
    }

    private function setLastName(string $lastName)
    {
        $this->lastName=$lastName;
    }

    public function lastName() : string
    {
      return $this->lastName;
    }

    public function __toString()
    {
      return $this->firstName().' '.$this->lastName();
    }
}
