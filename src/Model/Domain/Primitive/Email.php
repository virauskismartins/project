<?php

namespace Model\Domain\Primitive;

use Exception;

final class Email
{
    private $email;

    public function __construct($email)
    {
        $this->setEmail($email);
    }

    public function __construct($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          throw new Exeption('Invalid email provided');
        }

        $this->email=$email;

    }

    public function email() : string
    {
        return $this->email;
    }

    public function __toString()
    {
      return $this->email;
    }
}
