<?php
session_start();

$con = mysqli_connect(PDO_HOST, PDO_USERNAME, PDO_PASSWORD, PDO_DB);
if ( mysqli_connect_errno() ) {
  die ('Failed to connect to MySQL: ' . mysqli_connect_error());
}

if ( !isset($_POST['username'], $_POST['password']) ) {
  die ('Please fill both the username and password field!');
}

if ($stmt = $con->prepare('SELECT id, password FROM users WHERE username = ?')) {
  $stmt->bind_param('s', $_POST['username']);
  $stmt->execute();
  $stmt->store_result();

  if ($stmt->num_rows > 0) {
    $stmt->bind_result($id, $password);
    $stmt->fetch();

    if ($_POST['password'] === $password) {
      session_regenerate_id();
      $_SESSION['loggedin'] = TRUE;
      $_SESSION['name'] = $_POST['username'];
      $_SESSION['id'] = $id;
      header('Location: /cms');
    }
    else {
      echo 'Incorrect password!';
    }
    }
    else {
      echo 'Incorrect username!';
    }
    if (array_key_exists($stmt, $_COOKIE)) {
      $session_id = $_COOKIE[$stmt];
    }
}
