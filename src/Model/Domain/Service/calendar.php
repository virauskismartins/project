<?php

date_default_timezone_set('Europe/Riga');

$reslink = $_SERVER['REQUEST_URI'];

$linkdate = ltrim($reslink,'/');

$curr = date($linkdate);

if($reslink == "/$curr"){
  $curr = date($linkdate);
}

if($reslink < "/$curr"){
  $curr = date('Y-m',strtotime(' -1 month'));
}

if($reslink > "/$curr"){
  $curr = date('Y-m',strtotime(' +1 month'));
}

$today = date('Y-m-j', time());

$timestamp = strtotime($curr . '-01');
if ($timestamp === false) {
    //$curr = date('Y-m');
    $timestamp = strtotime($curr . '-01');
}
$html_title = date('m / Y', $timestamp);

// mktime(hour,minute,second,month,day,year)
$prev = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)-1, 1, date('Y', $timestamp)));
//$prev = date('Y-m');
$next = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)+1, 1, date('Y', $timestamp)));
//$next = date('Y-m');

$day_count = date('t', $timestamp);

$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 7, date('Y', $timestamp)));

$weeks = array();
$week = '';
$week .= str_repeat('<td></td>', $str);
$myBtn=1;
$dateDB=$curr.'-01';

$db = new mysqli(PDO_HOST, PDO_USERNAME, PDO_PASSWORD, PDO_DB);

$sql = "SELECT res_date FROM reservation";
$result = $db->query($sql);

$date_array = array();

if ($result->num_rows > 0){
  while($row = $result->fetch_assoc())
  {
    $date_array[] = $row["res_date"];
  }
}
$date_string = implode(",",$date_array);


for ($day = 1; $day <= $day_count; $day++, $str++, $dateDB++) {


    $date = $curr . '-' . $day;

    if ($today == $date) {
        $week .= '<td style="background-color: red;" class="today">' . $day;
    } else {

          if(strpos($date_string,$dateDB) != false){

                $week .= '<td class="calendar-dayy" style="background-color: red;" data-date="'.$dateDB.'" id="myBtn'.$day.'">' .$day;

          }
          else{
        $week .= '<td class="calendar-day" data-date="'.$dateDB.'" id="myBtn'.$day.'">' .$day;}
    }

    $week .= '</td>';

    if ($str % 7 == 6 || $day == $day_count) {
        if ($day == $day_count) {
            $week .= str_repeat('<td></td>', 6 - ($str % 7));
        }
        $weeks[] = '<tr>' . $week . '</tr>';
        $week = '';
    }
}
