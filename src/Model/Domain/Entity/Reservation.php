<?php
namespace Model\Domain\Entity;

use Exception;

class Reservation
{
  private $firstName;
  private $lastName;
  private $email;
  private $phone;
  private $resDate;
  private $guestCount;

  public function setFirstName($firstName)
  {

      if(!preg_match("/^([ā-žĀ-Ža-zA-Z' ]+)$/",$firstName)){

  require '/var/www/html/src/Model/Domain/Service/error.php';
  throw new Exception('Nepareizi ievadīts vārds');

  }

      $this->firstName=$firstName;
  }

  public function firstName()
  {
      return $this->firstName;
  }

  public function setLastName($lastName)
  {

      if(!preg_match("/^([ā-žĀ-Ža-zA-Z' ]+)$/",$lastName)){
        require '/var/www/html/src/Model/Domain/Service/error.php';
        throw new Exception('Nepareizi ievadīts uzvārds');
  }

      $this->lastName=$lastName;
  }

  public function lastName()
  {
      return $this->lastName;
  }

  public function setEmail($email)
  {
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      require '/var/www/html/src/Model/Domain/Service/error.php';
      throw new Exception('Invalid email entered');
    }
        $this->email=$email;
  }

  public function email()
  {
      return $this->email;
  }

  public function setPhone($phone)
  {
  if(!preg_match("/^[0-9]{8}$/", $phone)) {
    require '/var/www/html/src/Model/Domain/Service/error.php';
    throw new Exception('Numuram jāsatur 8 cipari');
  }

    $this->phone=$phone;
  }

  public function phone()
  {
      return $this->phone;
  }

  public function setResDate($resDate)
  {
    $today = date("Y-m-d");

      if($resDate <= $today) {
        require '/var/www/html/src/Model/Domain/Service/error.php';
        throw new Exception('Nevar rezervēt izvēlēto datumu');
      }
        $conn = mysqli_connect(PDO_HOST, PDO_USERNAME, PDO_PASSWORD, PDO_DB);

        $query = mysqli_query($conn, "SELECT * FROM reservation WHERE res_date='$resDate'");
         if(!mysqli_num_rows($query) == 0){
           require '/var/www/html/src/Model/Domain/Service/error.php';
           throw new Exception('Datums jau ir rezervēts');
         }

      $this->resDate=$resDate;
  }

  public function resDate()
  {
      return $this->resDate;
  }

  public function setGuestCount($guestCount)
  {
      if($guestCount <= 0 || $guestCount >= 31){
        require '/var/www/html/src/Model/Domain/Service/error.php';
        throw new Exception('Ievadīts nekorekts viesu skaits izvēlieties viesu skaitu no 1 līdz 30');
      }

      $this->guestCount=$guestCount;
  }

  public function guestCount()
  {
      return $this->guestCount;
  }
}
